# Entity Browser Vertical

## Overview

This module provides a new Widget Display plugin that will show labels of
referenced entities, but stack them vertically using CSS.

This is not a fully-featured tabledrag element, but it looks similar, with
comparable functionality. This can be seen as a simplified version of the patch
in https://www.drupal.org/project/entity_browser/issues/2973457 , and may
become obsolete if/when that patch lands in Entity Browser.

## Configuration

After installing the module, edit your Entity Browser widget settings in the
form display configuration form, and select "Entity label, stacked vertically"
as the display plugin.
